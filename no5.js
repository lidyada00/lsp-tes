function sumArrayElements(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
      sum += parseInt(arr[i]);
    }
    return sum;
  }
  
  const inputArray = [1, 2, 3, 4, 10, 11];
  const output = sumArrayElements(inputArray);
  
  console.log(output); // Output: 31

  // 4